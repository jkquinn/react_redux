import * as React from 'react';

export class CalcTool extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      value: 0,
    };
  }

  onChange = e => {
    this.setState({
      [ e.target.name ]: Number(e.target.value),
    });
  };
  
  render() {

    return <React.Fragment>
      <form>
        <div>
          <label>Input:</label>
          <input type="number" value={this.state.value}
            name="value" onChange={this.onChange} />
        </div>
        <div>
          <button type="button" onClick={() =>
            this.props.onAdd(this.state.value)}>+</button>
          <button type="button" onClick={() =>
            this.props.onSubtract(this.state.value)}>-</button>
          <button type="button" onClick={() =>
            this.props.onMultiply(this.state.value)}>*</button>
          <button type="button" onClick={() =>
            this.props.onDivide(this.state.value)}>/</button>
        </div>
        <div>
          Result: {this.props.result}
        </div>
      </form>
      <ul>
        {this.props.calcList.map(cl => <li>
          {cl.type} - {cl.value}
          <button type="button" onClick={() => this.props.onDeleteCalc(cl)}>X</button>
        </li>)}
      </ul>
    </React.Fragment>;
  }
}