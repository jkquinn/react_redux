import { bindActionCreators } from 'redux';

import { connect } from 'react-redux';

import {
  addActionCreator,
  subtractActionCreator,
  multiplyActionCreator,
  divideActionCreator,
} from '../actions/calc-actions';

import { CalcForm } from './CalcForm';

const mapStateToProps = ({ calcList }) => {

  const result = calcList.reduce( (acc, current) => {
    switch(current.type) {
      case 'ADD':
        return acc + current.value;
      case 'SUBTRACT':
        return acc - current.value;
      case 'MULTIPLY':
        return acc * current.value;
      case 'DIVIDE':
        return acc / current.value;
      default:
        return acc;
    }
  }, 0);

  return { result };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  onAdd: addActionCreator,
  onSubtract: subtractActionCreator,
  onMultiply: multiplyActionCreator,
  onDivide: divideActionCreator,
}, dispatch);

export const CalcFormContainer = connect(mapStateToProps, mapDispatchToProps)(CalcForm);
