import * as React from 'react';
import { bindActionCreators } from 'redux';

import {
  addActionCreator,
  subtractActionCreator,
  multiplyActionCreator,
  divideActionCreator
} from '../actions/calc-actions';
import { CalcTool } from './CalcTool';

export class CalcToolContainer extends React.Component {

  constructor(props) {
    super(props);

    this.actions = bindActionCreators({
      add: addActionCreator,
      subtract: subtractActionCreator,
      multiply: multiplyActionCreator,
      divide: divideActionCreator,
    }, props.store.dispatch);
  }

  componentDidMount() {
    this.unsubscribeStore = this.props.store.subscribe(() => {
      this.forceUpdate();
    });
  }

  componentWillUnmount() {
    this.unsubscribeStore();
  }

  render() {
    return <CalcTool result={this.props.store.getState()}
      onAdd={this.actions.add}
      onSubtract={this.actions.subtract}
      onMultiply={this.actions.multiply}
      onDivide={this.actions.divide} />; 
  }
}