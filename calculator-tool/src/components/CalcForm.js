import * as React from 'react';

export class CalcForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      value: 0,
    };
  }

  onChange = e => {
    this.setState({
      [ e.target.name ]: Number(e.target.value),
    });
  };
  
  render() {

    return <form>
      <div>
        <label>Input:</label>
        <input type="number" value={this.state.value}
          name="value" onChange={this.onChange} />
      </div>
      <div>
        <button type="button" onClick={() =>
          this.props.onAdd(this.state.value)}>+</button>
        <button type="button" onClick={() =>
          this.props.onSubtract(this.state.value)}>-</button>
        <button type="button" onClick={() =>
          this.props.onMultiply(this.state.value)}>*</button>
        <button type="button" onClick={() =>
          this.props.onDivide(this.state.value)}>/</button>
      </div>
      <div>
        Result: {this.props.result}
      </div>
    </form>;
  }
}