import * as React from 'react';

import { GiftListNameForm } from './GiftListNameForm';
import { GiftForm } from './GiftForm';

export class GiftTool extends React.Component {

  componentDidMount() {
    this.props.loadAllOptions()
      .then(() => this.props.refreshGifts());
  }

  render() {

    return <React.Fragment>
      {this.props.editingGiftListName
        ? <GiftListNameForm onSave={this.props.saveGiftListName} giftListName={this.props.giftListName} />
        : <div>
            <h1>
              {this.props.giftListName}
              <a className="tool-link" onClick={() => this.props.editGiftListName()}>[Edit]</a>
            </h1>
            
        </div>}
      <ul>{this.props.items.map(i =>
        <li>
          {i.name} <span className={ this.props.options.getCurrencySymbol() + '-currency' }>{i.price}</span>
          <button type="button" className="del-button" onClick={() => this.props.remove(i.id)}>X</button>
        </li>)}</ul>
      <GiftForm onSubmitGift={gift =>
        this.props.add(gift).then(() =>
          this.props.refreshGifts())} />
    </React.Fragment>;
  }
}