import * as React from 'react';
import { bindActionCreators } from 'redux';

import { GiftForm } from './GiftForm';

const addGiftActionCreator = gift =>
  ({ type: 'ADD_GIFT', gift });
const removeGiftActionCreator = giftName =>
  ({ type: 'REMOVE_GIFT', giftName });

export class GiftListContainer extends React.Component {

  constructor(props) {
    super(props);

    this.actions = bindActionCreators({
      addGift: addGiftActionCreator,
      removeGift: removeGiftActionCreator,
    }, this.props.store.dispatch)
  }

  componentDidMount() {
    this.unsubscribeStore = this.props.store.subscribe(() => {
      this.forceUpdate();
    });
  }

  componentWillUnmount() {
    this.unsubscribeStore();
  }

  render() {
    return <GiftList gifts={this.props.store.getState()}
      onAddGift={this.actions.addGift}
      onRemoveGift={this.actions.removeGift} />
  }

}

export const GiftList = props =>
  <React.Fragment>
    <ul>
      {props.gifts.map(gift => <li>
        {gift.name} {gift.price}
        <button type="button" onClick={() => props.onRemoveGift(gift.name)}>
          Remove
        </button>
      </li>)}
    </ul>
    <GiftForm onSubmitGift={props.onAddGift} />
  </React.Fragment>;

