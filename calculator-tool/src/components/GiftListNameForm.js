import * as React from 'react';

export class GiftListNameForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      giftListName: props.giftListName,
    };
  }

  onChange = e => {
    this.setState({
      [ e.target.name ]: e.target.type === 'number'
        ? Number(e.target.value) : e.target.value
    });
  }

  render() {

    return <form className="tool-form">
        <input type="text" name="giftListName"
          value={this.state.giftListName} onChange={this.onChange} />
        <a className="tool-link" onClick={() => this.props.onSave(this.state.giftListName)}>[Save]</a>
    </form>

  }


}