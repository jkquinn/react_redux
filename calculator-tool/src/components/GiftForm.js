import * as React from 'react';

export class GiftForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      price: 0,
    };
  }

  onChange = e => {
    this.setState({
      [ e.target.name ]: e.target.type === 'number'
        ? Number(e.target.value) : e.target.value
    });
  }

  render() {

    return <form>
      <div className="form-row">
        <label>Gift Name:</label>
        <input type="text" name="name"
          value={this.state.name} onChange={this.onChange} />
      </div>
      <div className="form-row">
        <label>Gift Price:</label>
        <input type="text" name="price"
          value={this.state.price} onChange={this.onChange} />
      </div>
      <button type="button" onClick={() => this.props.onSubmitGift({
        ...this.state
      })}>Submit</button>
    </form>

  }


}