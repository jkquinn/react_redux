import * as React from 'react';
import { bindActionCreators } from 'redux';

import { connect } from 'react-redux';
import {
  deleteCalcActionCreator,
} from '../actions/calc-actions';

export const CalcListContainer = connect(
  ({ calcList}) => ({ calcList }),
  dispatch => bindActionCreators({ onDeleteCalc: deleteCalcActionCreator }, dispatch),
)(props => <ul>
  {props.calcList.map(c => <li>
    {c.type} - {c.value}
    <button type="button" onClick={() => props.onDeleteCalc(c)}>X</button>
  </li>)}
</ul>);

