import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { refreshGifts } from '../actions/refresh-gifts';
import { refreshOptions } from '../actions/refresh-options';
import { addGift } from '../actions/add-gift';
import { removeGift } from '../actions/remove-gift';
import { editGiftListNameActionCreator, saveGiftListName } from '../actions/gift-list';
import { GiftToolOptions } from '../models/GiftToolOptions';

import { GiftTool } from './GiftTool';

export const GiftToolContainer = connect(
  // mapStateToProps - data from the state and maps it to props passed into the component
  ({ gifts, editingGiftListName, giftListName, options }) => {

    const gtOptions = new GiftToolOptions(options);

    return {
      items: gifts,
      editingGiftListName,
      giftListName: gtOptions.getGiftListName(),
      options: gtOptions
    };
  },
  // mapDispatchToProps - bind action creators to the store's dispatch function and passed them in as props
  // to the component
  dispatch => bindActionCreators({
    refreshGifts, add: addGift, remove: removeGift, saveGiftListName,
    loadAllOptions: refreshOptions,
    editGiftListName: editGiftListNameActionCreator,
  }, dispatch),
)(GiftTool);
