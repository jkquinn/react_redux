import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { createStore, bindActionCreators } from 'redux';

import { CalcTool } from './components/CalcTool';

const calcReducer = (state = 0, action) => {
  switch (action.type) {
    case 'ADD':
      console.log('add: ', action.value)
      return state + action.value;
    case 'SUBTRACT':
    console.log('subtract: ', action.value)
    return state - action.value;
    default:
      return state;
  }
};

const createStore = (reducerFn, initialState) => {

  let currentState = initialState;
  const subscribers = [];

  return {
    getState: () => currentState,
    dispatch: action => {
      currentState = reducerFn(currentState, action);
      subscribers.forEach(cb => cb());
    },
    subscribe: cb => {
      subscribers.push(cb);
    },
  };

};

const appStore = createStore(calcReducer, 0);

appStore.subscribe(() => {
  console.log(appStore.getState());
});

const addActionCreator = value => ({ type: 'ADD', value });
const subtractActionCreator = value => ({ type: 'SUBTRACT', value });

// const bindActionCreators = (actionCreators, dispatch) => {

//   return Object.keys(actionCreators).reduce( (actions, key) => {
//     actions[key] = (...params) => dispatch(actionCreators[key](...params));
//     return actions;
//   }, {});

// };

const { add, subtract } = bindActionCreators({
  add: addActionCreator,
  subtract: subtractActionCreator
}, appStore.dispatch);

// const add = value => {
//   appStore.dispatch(addActionCreator(value));
// };

// const subtract = value => {
//   appStore.dispatch(subtractActionCreator(value));
// };


ReactDOM.render(<CalcTool
  result={appStore.getState()}
  add={add}
  subtract={subtract}
/>, document.querySelector('#root'));

appStore.subscribe(() => {
  ReactDOM.render(<CalcTool
    result={appStore.getState()}
    add={add}
    subtract={subtract}
  />, document.querySelector('#root'));
});

