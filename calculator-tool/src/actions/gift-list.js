import { actionTypes } from '../giftActionTypes';
import {
  GIFT_LIST_NAME_OPTION_ID,
  GIFT_LIST_NAME_OPTION_NAME,
} from '../gift-tool-constants';

export const editGiftListNameActionCreator = () =>
  ({ type: actionTypes.EDIT_GIFT_LIST_NAME });

export const saveGiftListNameRequestActionCreator = giftListName =>
  ({ type: actionTypes.SAVE_GIFT_LIST_NAME_REQUEST, giftListName });
export const saveGiftListNameDoneActionCreator = giftListName =>
  ({ type: actionTypes.SAVE_GIFT_LIST_NAME_DONE, giftListName });



export const saveGiftListName = giftListName => {
  return dispatch => {
    dispatch(saveGiftListNameRequestActionCreator(giftListName));
    return fetch('http://localhost:3050/gift_tool_options/' + GIFT_LIST_NAME_OPTION_ID, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        id: GIFT_LIST_NAME_OPTION_ID,
        option_name: GIFT_LIST_NAME_OPTION_NAME,
        option_value: giftListName
      }),
    })
      .then(() => dispatch(saveGiftListNameDoneActionCreator(giftListName)));
  };
};