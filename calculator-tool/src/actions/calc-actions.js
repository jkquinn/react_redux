export const addActionCreator = value =>
  ({ type: 'ADD', value });
export const subtractActionCreator = value =>
  ({ type: 'SUBTRACT', value });
export const multiplyActionCreator = value =>
  ({ type: 'MULTIPLY', value });
export const divideActionCreator = value =>
  ({ type: 'DIVIDE', value });
export const deleteCalcActionCreator = calc =>
  ({ type: 'DELETE_CALC', calc });
