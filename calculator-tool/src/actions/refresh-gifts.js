import  { actionTypes } from '../giftActionTypes';

export const refreshGiftsRequestActionCreator = () => ({ type: actionTypes.REFRESH_GIFTS_REQUEST });
export const refreshGiftsDoneActionCreator = gifts => ({ type: actionTypes.REFRESH_GIFTS_DONE, gifts });

export const refreshGifts = () => {
  return dispatch => {
    dispatch(refreshGiftsRequestActionCreator());
    return fetch('http://localhost:3050/gifts')
      .then(res => res.json())
      .then(gifts => dispatch(refreshGiftsDoneActionCreator(gifts)));
  };
};