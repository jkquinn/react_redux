import  { actionTypes } from '../giftActionTypes';

export const refreshOptionsRequestActionCreator = () => ({ type: actionTypes.REFRESH_OPTIONS_REQUEST });
export const refreshOptionsDoneActionCreator = options => ({ type: actionTypes.REFRESH_OPTIONS_DONE, options });

export const refreshOptions = () => {
  return dispatch => {
    dispatch(refreshOptionsRequestActionCreator());
    return fetch('http://localhost:3050/gift_tool_options')
      .then(res => res.json())
      .then(options => dispatch(refreshOptionsDoneActionCreator(options)));
  };
};