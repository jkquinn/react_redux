import { actionTypes as giftActionTypes } from '../giftActionTypes';

export const removeGiftRequestActionCreator = giftId => ({ type: giftActionTypes.REMOVE_GIFT_REQUEST, giftId });
export const removeGiftDoneActionCreator = giftId => ({ type: giftActionTypes.REMOVE_GIFT_DONE, giftId });

export const removeGift = giftId => {
  return dispatch => {
    dispatch(removeGiftRequestActionCreator(giftId));
    return fetch('http://localhost:3050/gifts/' + encodeURIComponent(giftId), {
      method: 'DELETE',
    })
      .then(gift => dispatch(removeGiftDoneActionCreator(giftId)));
  };
};