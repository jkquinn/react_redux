import { actionTypes as giftActionTypes } from '../giftActionTypes';

export const addGiftRequestActionCreator = () => ({ type: giftActionTypes.ADD_GIFT_REQUEST });
export const addGiftDoneActionCreator = gift => ({ type: giftActionTypes.ADD_GIFT_DONE, gift });

export const addGift = gift => {
  return dispatch => {
    dispatch(addGiftRequestActionCreator());
    return fetch('http://localhost:3050/gifts', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(gift),
    })
      .then(res => res.json())
      .then(gift => dispatch(addGiftDoneActionCreator(gift)));
  };
};