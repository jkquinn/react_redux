import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Button } from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

ReactDOM.render(<form>
  <Button color="primary">Do Something</Button>
</form>, document.querySelector('#root'));