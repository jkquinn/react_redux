import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { createStore, bindActionCreators, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { connect, Provider } from 'react-redux';
import { withRouter, BrowserRouter as Router, Link, Route } from 'react-router-dom';
import { composeWithDevTools } from 'redux-devtools-extension';

const REFRESH_GIFT_LIST_REQUEST = 'REFRESH_GIFT_LIST_REQUEST';
const REFRESH_GIFT_LIST_DONE = 'REFRESH_GIFT_LIST_DONE';

const REFRESH_GIFT_TOOL_OPTIONS_REQUEST = 'REFRESH_GIFT_TOOL_OPTIONS_REQUEST';
const REFRESH_GIFT_TOOL_OPTIONS_DONE = 'REFRESH_GIFT_TOOL_OPTIONS_DONE';

const refreshGiftListRequestActionCreator = () => ({ type: REFRESH_GIFT_LIST_REQUEST });
const refreshGiftListDoneActionCreator = gifts => ({ type: REFRESH_GIFT_LIST_DONE, gifts });

const refreshGiftToolOptionsRequestActionCreator = () => ({ type: REFRESH_GIFT_TOOL_OPTIONS_REQUEST });
const refreshGiftToolOptionsDoneActionCreator = options => ({ type: REFRESH_GIFT_TOOL_OPTIONS_DONE, options });

const refreshGiftList = () => {
  return dispatch => {
    dispatch(refreshGiftListRequestActionCreator());
    return fetch('http://localhost:3050/gifts')
      .then(res => res.json())
      .then(gifts => dispatch(refreshGiftListDoneActionCreator(gifts)));
  };
};

const refreshGiftToolOptions = () => {
  return dispatch => {
    dispatch(refreshGiftToolOptionsRequestActionCreator());
    return fetch('http://localhost:3050/gifts')
      .then(res => res.json())
      .then(options => {
        return new Promise(resolve => {
          setTimeout(() => {
            resolve(dispatch(refreshGiftToolOptionsDoneActionCreator(options)));
          }, 2000);
        });
      });
  };
};

const giftListReducer = (state = { gifts: [], giftListName: '' }, action) => {
  switch (action.type) {
    case REFRESH_GIFT_LIST_REQUEST:
      return state;
    case REFRESH_GIFT_LIST_DONE:
      return { ...state, gifts: action.gifts };
    default:
      return state;
  }
};

const giftToolOptionsReducer = (state = { options: [] }, action) => {
  switch (action.type) {
    case REFRESH_GIFT_TOOL_OPTIONS_REQUEST:
      return state;
    case REFRESH_GIFT_TOOL_OPTIONS_DONE:
      return { ...state, options: action.options };
    default:
      return state;
  }
};

const routeReducers = combineReducers({
  giftList: giftListReducer,
  giftToolOptions: giftToolOptionsReducer
});

const giftToolReducer = (state = { loading: false }, action) => {
  return routeReducers(action.type.endsWith('REQUEST')
    ? { ...state, loading: true }
    : { ...state, loading: false }, action);
}

const appStore = createStore(giftToolReducer, composeWithDevTools(applyMiddleware(thunk)));

class GiftList extends React.Component {

  componentDidMount() {
    this.props.refreshGiftList();
  }

  render() {
    return "Gift List: " + this.props.gifts.length;
  }
}

const GiftListContainer = withRouter(connect(
  ({ giftList: { gifts } }) => ({ gifts }),
  dispatch => bindActionCreators({ refreshGiftList }, dispatch),
)(GiftList));

class GiftToolOptions extends React.Component {

  componentDidMount() {
    this.props.refreshGiftToolOptions();
  }

  render() {
    return "Gift Tool Options: " + this.props.options.length;
  }
}

const GiftToolOptionsContainer = withRouter(connect(
  ({ giftToolOptions: { options } }) => ({ options }),
  dispatch => bindActionCreators({ refreshGiftToolOptions }, dispatch),
)(GiftToolOptions));

class GiftTool extends React.Component {

  render() {

    return <React.Fragment>
      <nav>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/options">Options</Link></li>
        </ul>
      </nav>
      <div>{this.props.loading ? "Loading..." : "Loaded."}</div>
      <Route exact path="/" component={GiftListContainer} />
      <Route exact path="/options" component={GiftToolOptionsContainer} />
    </React.Fragment>;
  }
}

const GiftToolContainer = withRouter(connect(
  ({ loading }) => ({ loading }),
  dispatch => {},
)(GiftTool));

ReactDOM.render(<Provider store={appStore}><Router>
  <GiftToolContainer />
</Router></Provider>, document.querySelector('#root'));

