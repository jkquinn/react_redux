import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import { calcReducer } from './reducers/calc-reducer';

import { CalcFormContainer } from './components/CalcFormContainer';
import { CalcListContainer } from './components/CalcListContainer';

const appStore = createStore(calcReducer, composeWithDevTools());

ReactDOM.render(
  <Provider store={appStore}>
    <React.Fragment>
      <CalcFormContainer />
      <CalcListContainer />
    </React.Fragment>
  </Provider>,
  document.querySelector('#root')
);
