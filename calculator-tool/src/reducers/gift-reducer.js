import { actionTypes } from '../giftActionTypes';
import { GiftToolOptions } from '../models/GiftToolOptions';

const initialState = {
  loading: false,
  editingGiftListName: false,
  gifts: [],
  options: [],
};

export const giftReducer = (state = initialState, action) => {

  switch(action.type) {
    case actionTypes.REFRESH_GIFTS_REQUEST:
      return { ...state, loading: true };
    case actionTypes.REFRESH_GIFTS_DONE:

      const gtOptions = new GiftToolOptions(state.options);

      const sortFieldName = gtOptions.getDefaultSortField();

      return { ...state, loading: false, gifts: action.gifts.concat().sort( (a,b) => {

        if (a[sortFieldName] < b[sortFieldName]) {
          return -1;
        } else if (a[sortFieldName] > b[sortFieldName]) { 
          return 1;
        } else {
          return 0;
        }

      } ) };

    case actionTypes.ADD_GIFT_REQUEST:
      return { ...state, loading: true };
    case actionTypes.ADD_GIFT_DONE:
      return { ...state, loading: false, gifts: state.gifts.concat(action.gift) };
    case actionTypes.REMOVE_GIFT_REQUEST:
      return { ...state, loading: true };
    case actionTypes.REMOVE_GIFT_DONE:
      return { ...state, loading: false, gifts: state.gifts.filter(g => g.id !== action.giftId) };
    case actionTypes.REFRESH_OPTIONS_REQUEST:
      return { ...state, loading: true };
    case actionTypes.REFRESH_OPTIONS_DONE:
      return {
        ...state,
        loading: false,
        options: action.options,
      };
    case actionTypes.EDIT_GIFT_LIST_NAME:
      return { ...state, editingGiftListName: true };
    case actionTypes.SAVE_GIFT_LIST_NAME_REQUEST:
      return { ...state, loading: true };
    case actionTypes.SAVE_GIFT_LIST_NAME_DONE:
      return { ...state, loading: false, editingGiftListName: false, giftListName: action.giftListName };
    default:
      return state;
  }
};