export const giftReducer = (state = [], action) => {
  switch (action.type) {
    case 'ADD_GIFT':
      return state.concat(action.gift);
    case 'REMOVE_GIFT':
      return state.filter(g => g.name !== action.giftName);
    default:
      return state;
  }
};