
// New State Structure:

// {
//   result: <some value>,
//   calculationList: [
//     { type: 'ADD', value: <some value> },
//     { type: 'SUBTRACT', value: <some value> },
//     { type: 'MULTIPLY', value: <some value> },
//     { type: 'ADD', value: <some value> },
//     { type: 'ADD', value: <some value> },
//   ]
// }

// Exercise #7

// 1. Update the reducer to use the new state tree structure defined above
// 2. Capture each calculation in a list of calculations.
// 3. Under the calctool form display a list of calculations performed
const initialState = {
  calcList: [],
}

const appendCalc = (state, action) => {
  return {
    ...state,
    calcList: state.calcList.concat({ ...action }),
  };
}

export const calcReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD':
    case 'SUBTRACT':
    case 'MULTIPLY':
    case 'DIVIDE':
      return appendCalc(state, action);
    case 'DELETE_CALC':
      return { ...state, calcList: state.calcList.filter(c => c !== action.calc) }
    default:
      return state;
  }
};