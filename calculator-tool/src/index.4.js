import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { createStore, bindActionCreators, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { connect, Provider } from 'react-redux';

import { actionTypes } from './giftActionTypes';

import { refreshGifts } from './actions/refresh-gifts';
import { refreshOptions } from './actions/refresh-options';

import { GiftListNameForm } from './components/GiftListNameForm';
import { GiftForm } from './components/GiftForm';
import { GiftTool } from './components/GiftTool';

const initialState = {
  loading: false,
  giftListName: 'New List',
  editingGiftListName: false,
  gifts: []
};


const giftReducer = (state = initialState, action) => {

  switch(action.type) {
    case actionTypes.REFRESH_REQUEST:
      return { ...state, loading: true };
    case actionTypes.REFRESH_DONE:
      return { ...state, loading: false, gifts: action.gifts };
    case actionTypes.ADD_REQUEST:
      return { ...state, loading: true };
    case actionTypes.ADD_DONE:
      return { ...state, loading: false, gifts: state.gifts.concat(action.gift) };
    case actionTypes.REMOVE_REQUEST:
      return { ...state, loading: true };
    case actionTypes.REMOVE_DONE:
      return { ...state, loading: false, gifts: state.gifts.filter(g => g.id !== action.giftId) };
    case actionTypes.EDIT_GIFT_LIST_NAME:
      return { ...state, editingGiftListName: true };
    case actionTypes.SAVE_GIFT_LIST_NAME_REQUEST:
      return { ...state, loading: true };
    case actionTypes.SAVE_GIFT_LIST_NAME_DONE:
      return { ...state, loading: false, editingGiftListName: false, giftListName: action.giftListName };
    default:
      return state;
  }
};

const appStore = createStore(giftReducer, composeWithDevTools(applyMiddleware(thunk)));



const addRequestActionCreator = () => ({ type: 'ADD_REQUEST' });
const addDoneActionCreator = gift => ({ type: 'ADD_DONE', gift });

const add = gift => {
  return dispatch => {
    dispatch(addRequestActionCreator());
    return fetch('http://localhost:3050/gifts', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(gift),
    })
      .then(res => res.json())
      .then(gift => dispatch(addDoneActionCreator(gift)))
      .then(() => refresh()(dispatch));
  };
};

const removeRequestActionCreator = giftId => ({ type: 'REMOVE_REQUEST', giftId });
const removeDoneActionCreator = giftId => ({ type: 'REMOVE_DONE', giftId });

const remove = giftId => {
  return dispatch => {
    dispatch(removeRequestActionCreator(giftId));
    return fetch('http://localhost:3050/gifts/' + encodeURIComponent(giftId), {
      method: 'DELETE',
    })
      .then(gift => dispatch(removeDoneActionCreator(giftId)))
      .then(() => refresh()(dispatch));
  };
};

const editGiftListNameActionCreator = () => ({ type: 'EDIT_GIFT_LIST_NAME' });

const saveGiftListNameRequestActionCreator = giftListName => ({ type: 'SAVE_GIFT_LIST_NAME_REQUEST', giftListName });
const saveGiftListNameDoneActionCreator = giftListName => ({ type: 'SAVE_GIFT_LIST_NAME_DONE', giftListName });

const GIFT_LIST_NAME_OPTION_ID = 1;
const GIFT_LIST_NAME_OPTION_NAME = 'gift_list_name';

const saveGiftListName = giftListName => {
  return dispatch => {
    dispatch(saveGiftListNameRequestActionCreator(giftListName));
    return fetch('http://localhost:3050/gift_tool_options/' + GIFT_LIST_NAME_OPTION_ID, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        id: GIFT_LIST_NAME_OPTION_ID,
        option_name: GIFT_LIST_NAME_OPTION_NAME,
        option_value: giftListName
      }),
    })
      .then(() => dispatch(saveGiftListNameDoneActionCreator(giftListName)));
  };
};

const GiftToolContainer = connect(
  // mapStateToProps - data from the state and maps it to props passed into the component
  ({ gifts, editingGiftListName, giftListName }) => ({ items: gifts, editingGiftListName, giftListName }),
  // mapDispatchToProps - bind action creators to the store's dispatch function and passed them in as props
  // to the component
  dispatch => bindActionCreators({
    refreshGifts, add, remove, saveGiftListName,
    loadAllOptions: refreshOptions,
    editGiftListName: editGiftListNameActionCreator,
  }, dispatch),
)(GiftTool);

ReactDOM.render(<Provider store={appStore}>
  <GiftToolContainer />
</Provider>, document.querySelector('#root'));