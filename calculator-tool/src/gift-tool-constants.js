export const GIFT_LIST_NAME_OPTION_ID = 1;
export const GIFT_LIST_NAME_OPTION_NAME = 'gift_list_name';

export const DEFAULT_SORT_FIELD_OPTION_ID = 2;
export const DEFAULT_SORT_FIELD_OPTION_NAME = 'default_sort_field';

export const CURRENCY_SYMBOL_OPTION_ID = 3;
export const CURRENCY_SYMBOL_OPTION_NAME = 'currency_symbol';