import keyMirror from 'key-mirror';

export const actionTypes = keyMirror({
  REFRESH_GIFTS_REQUEST: null,
  REFRESH_GIFTS_DONE: null,
  ADD_GIFT_REQUEST: null,
  ADD_GIFT_DONE: null,
  REMOVE_GIFT_REQUEST: null,
  REMOVE_GIFT_DONE: null,
  REFRESH_OPTIONS_REQUEST: null,
  REFRESH_OPTIONS_DONE: null,
  EDIT_GIFT_LIST_NAME: null,
  SAVE_GIFT_LIST_NAME_REQUEST: null,
  SAVE_GIFT_LIST_NAME_DONE: null,
});

// actionTypes = {
//   REFRESH_REQUEST: 'REFRESH_REQUEST',
//   REFRESH_DONE: 'REFRESH_DONE',
//   ADD_REQUEST: 'ADD_REQUEST',
//   ...
// }