import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { appStore } from './gift-tool-store';
import { GiftToolContainer } from './components/GiftToolContainer';

import './index.css';

ReactDOM.render(<Provider store={appStore}>
  <GiftToolContainer />
</Provider>, document.querySelector('#root'));