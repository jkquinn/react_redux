import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { giftReducer } from './reducers/gift-reducer';

export const appStore = createStore(
  giftReducer,
  composeWithDevTools(applyMiddleware(thunk))
);
