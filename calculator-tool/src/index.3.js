import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { createStore } from 'redux';

import { GiftListContainer } from './components/GiftList';
import { giftReducer } from './reducers/gift-reducer';

const appStore = createStore(giftReducer);

ReactDOM.render(
  <GiftListContainer store={appStore} />,
  document.querySelector('#root')
);
