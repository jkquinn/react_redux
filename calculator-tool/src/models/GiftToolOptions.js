import {
  GIFT_LIST_NAME_OPTION_ID,
  DEFAULT_SORT_FIELD_OPTION_ID,
  CURRENCY_SYMBOL_OPTION_ID,
} from '../gift-tool-constants';

export class GiftToolOptions {

  constructor(options) {
    this._options = options;
  }

  _findOptionValue(optionId) {
    if (!this._options) return;

    const option = this._options.find(o => o.id === optionId);
    if (!option) return;

    return option.option_value
  }

  getGiftListName() {
    return this._findOptionValue(GIFT_LIST_NAME_OPTION_ID)
  }

  getDefaultSortField() {
    return this._findOptionValue(DEFAULT_SORT_FIELD_OPTION_ID)
  }

  getCurrencySymbol() {
    return this._findOptionValue(CURRENCY_SYMBOL_OPTION_ID)
  }

}