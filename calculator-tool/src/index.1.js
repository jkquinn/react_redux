import React from 'react';
import ReactDOM from 'react-dom';

import { CalcTool } from './components/CalcTool';

ReactDOM.render(
  <CalcTool />,
  document.getElementById('root')
);
