# Welcome to React & Redux

## Instructor

Eric Greene

## Schedule

Class:

- Monday - Friday: 8:00am to 4:00pm

Breaks:

- Morning: 9:45am to 10:00am
- Lunch: 11:30am to 12:30pm
- Afternoon: 2:15pm to 2:30pm

## Course Outline

JavaScript topics will be included as needed into the React & Redux content:

- Day 1 - React Review, Patterns, Redux, NPM
- Day 2 - Redux Continued, Fetch/Promise Review, Redux Thunk
- Day 3 - Redux Thunk
- Day 4 - Project
- Day 5 - React Lifecycle, Unit Testing

## Links

### Instructor's Resources

- [Accelebrate, Inc.](https://www.accelebrate.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016
- [Microsoft Virtual Academy](https://mva.microsoft.com/search/SearchResults.aspx#!q=Eric%20Greene&lang=1033)
- [React SitePoint](http://www.sitepoint.com/author/ericgreene/)

### Other Resources

- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
- [JavaScript Air Podcast](http://javascriptair.podbean.com/)
- [Speaking JavaScript](http://speakingjs.com/es5/)
- [Redux Videos](https://egghead.io/courses/getting-started-with-redux)
- [TC39 Process - Feature Stages](http://www.2ality.com/2015/11/tc39-process.html)
- [Official ES2016 Features](http://www.2ality.com/2016/01/ecmascript-2016.html)
- [Possible ES2017 Features](http://www.2ality.com/2016/02/ecmascript-2017.html)

## Useful Resources

- [React](https://facebook.github.io/react/)
- [Redux](https://github.com/reactjs/redux)
- [React-Redux](https://github.com/reactjs/react-redux)
- [Redux-Thunk](https://github.com/gaearon/redux-thunk)
- [Babel](https://babeljs.io/)
- [Webpack](https://webpack.github.io/)
- [SASS](http://sass-lang.com/)
- [Bootstrap](https://v4-alpha.getbootstrap.com/)